import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  NativeModules,
  Platform,
  Image,
  Dimensions,
  FlatList,
  ListRenderItemInfo,
  TouchableOpacity,
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { shippingSVG } from './assets/svg/shipping';
import {
  Inter_900Black,
  Inter_100Thin,
  Inter_200ExtraLight,
  Inter_300Light,
  Inter_400Regular,
  Inter_500Medium,
  Inter_600SemiBold,
  Inter_700Bold,
  Inter_800ExtraBold,
  useFonts,
} from '@expo-google-fonts/inter';
const { StatusBarManager } = NativeModules;
var win = Dimensions.get('window');
const width = Image.resolveAssetSource(
  require('./assets/image/watch.jpg')
).width;
const height = Image.resolveAssetSource(
  require('./assets/image/watch.jpg')
).height;
const ratio = win.width / width;
interface IItemFeatue {
  key: number;
  value: string;
}
const listFeature1: IItemFeatue[] = [
  {
    key: 1,
    value: 'Chronograph',
  },
  {
    key: 2,
    value: 'Master Chronometer Certified',
  },
  {
    key: 3,
    value: 'Tachymeter',
  },
];
const listFeature2: IItemFeatue[] = [
  {
    key: 1,
    value: 'Anti‑magnetic',
  },
  {
    key: 2,
    value: 'Chronometer',
  },
  {
    key: 3,
    value: 'Small seconds',
  },
];
interface IItemDetail {
  title: string;
  value: string;
}
const productDetails: IItemDetail[] = [
  {
    title: 'Between lugs',
    value: '20 mm',
  },
  {
    title: 'Bracelet',
    value: 'leather strap',
  },
  {
    title: 'Case',
    value: 'Steel',
  },
  {
    title: 'Case diameter',
    value: '42 mm',
  },
  {
    title: 'Dial color',
    value: 'Black',
  },
  {
    title: 'Crystal',
    value:
      'Domed, scratch‑resistant sapphire crystal with anti‑reflective treatment inside',
  },
  {
    title: 'Water resistance',
    value: '5 bar (50 metres / 167 feet)',
  },
];
const Seperator: React.FC = () => {
  return <View style={styles.seperator} />;
};
const ItemFeature: React.FC<IItemFeatue> = ({ value, key }) => {
  return (
    <Text style={[styles.itemFeature, key === 1 ? styles.mt0 : undefined]}>
      {value}
    </Text>
  );
};
const ItemDetail: React.FC<IItemDetail> = ({ title, value }) => {
  return (
    <View style={styles.itemDetailWrapper}>
      <Text>
        <Text style={styles.detailTitle}>{title}:</Text> {value}
      </Text>
    </View>
  );
};
export default function App() {
  let [fontsLoaded] = useFonts({
    Inter_100Thin,
    Inter_200ExtraLight,
    Inter_300Light,
    Inter_400Regular,
    Inter_500Medium,
    Inter_600SemiBold,
    Inter_700Bold,
    Inter_800ExtraBold,
  });

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <Image
          style={styles.image}
          resizeMode='contain'
          source={require('./assets/image/watch.jpg')}
        />
        <Text style={styles.name}>Automatic Watch</Text>
        <Text style={styles.price}>$350.00 USD</Text>
        <Text style={styles.description}>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore
        </Text>
        <Text style={styles.subDescription}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid
          amet at delectus doloribus dolorum expedita hic, ipsum maxime modi nam
          officiis porro, quae, quisquam quos reprehenderit velit? Natus, totam.
        </Text>
        <Seperator />
        <Text style={styles.title}>Feature</Text>
        <FlatList
          data={listFeature1}
          renderItem={({ item }: ListRenderItemInfo<IItemFeatue>) => (
            <ItemFeature key={item.key} value={item.value} />
          )}
          keyExtractor={(item: IItemFeatue) => item.key.toString()}
        />
        <FlatList
          data={listFeature2}
          renderItem={({ item }: ListRenderItemInfo<IItemFeatue>) => (
            <ItemFeature key={item.key} value={item.value} />
          )}
          keyExtractor={(item: IItemFeatue) => item.key.toString()}
          style={styles.mt40}
        />
        <Seperator />
        <Text style={styles.title}>Product Details</Text>
        <FlatList
          data={productDetails}
          renderItem={({ item }: ListRenderItemInfo<IItemDetail>) => (
            <ItemDetail title={item.title} value={item.value} />
          )}
          keyExtractor={(item: IItemDetail) => item.title}
        />
        <TouchableOpacity style={styles.button} onPress={() => {}}>
          <Text style={styles.btnText}>ADD TO CART</Text>
        </TouchableOpacity>
        <View style={styles.shippingWrapper}>
          <SvgXml
            xml={shippingSVG}
            width={16}
            height={16}
            fill='#1a202c'
            style={styles.svg}
          />
          <Text>2-3 business days delivery</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT,
  },
  scrollView: {
    paddingHorizontal: 16,
  },
  image: {
    height: ratio * height,
    width: win.width - 32,
    borderRadius: 6,
  },
  seperator: {
    marginVertical: 16,
    borderBottomColor: '#E2E8F0',
    borderBottomWidth: 1,
  },
  title: {
    fontSize: 16,
    color: '#d69e2e',
    fontFamily: 'Inter_500Medium',
    textTransform: 'uppercase',
    marginBottom: 16,
  },
  itemFeature: {
    fontSize: 16,
    lineHeight: 22,
    color: '#1A202C',
    marginTop: 8,
  },
  mt0: {
    marginTop: 0,
  },
  mt40: {
    marginTop: 40,
  },
  name: {
    marginTop: 32,
    fontSize: 24,
    lineHeight: 26,
    fontFamily: 'Inter_600SemiBold',
    color: '#1a202c',
  },
  price: {
    fontSize: 24,
    fontFamily: 'Inter_300Light',
    color: '#171923',
  },
  description: {
    fontSize: 24,
    color: '#718096',
    marginTop: 24,
  },
  subDescription: {
    fontSize: 16,
    color: '#1a202c',
    marginTop: 16,
  },
  itemDetailWrapper: {
    marginTop: 16,
    fontSize: 16,
    color: '#1a202c',
    fontFamily: 'Inter_300Light',
  },
  'itemDetailWrapper:first-child': {
    marginTop: 0,
  },
  detailTitle: {
    fontFamily: 'Inter_700Bold',
  },
  button: {
    marginTop: 24,
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#171923',
  },
  btnText: {
    fontSize: 18,
    color: '#FFF',
  },
  shippingWrapper: {
    alignItems: 'center',
    marginTop: 24,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  svg: {
    marginRight: 8,
  },
});
